// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";
import { getFirestore } from "firebase/firestore";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyBgyzB6Pbg5oFMxIq1x9PrP76Bzc-LnXq8",
  authDomain: "quizappsocaai.firebaseapp.com",
  projectId: "quizappsocaai",
  storageBucket: "quizappsocaai.appspot.com",
  messagingSenderId: "156712878815",
  appId: "1:156712878815:web:882a90b032ba1d5b6b5f6a"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

export const auth = getAuth(app);
export const db = getFirestore(app);