import {
  signInWithEmailAndPassword,
  createUserWithEmailAndPassword,
  updateProfile,
} from "firebase/auth";
import { addDoc, collection } from "firebase/firestore";
import { db } from "./Index";
import { Alert } from "react-native";

// login
export const SignInMethod = (auth, email, password) => {
  signInWithEmailAndPassword(auth, email, password)
    .then((userCredentials) => {
      const user = userCredentials.user;
      Alert.alert("Alert", "Login Success");
    })
    .catch((error) => {
      console.log(error);
      Alert.alert("Login Failed", error.message);
    });
};

//register
export const SignUpMethod = (auth, fullName, email, password) => {
  createUserWithEmailAndPassword(auth, email, password)
    .then((userCredentials) => {
      updateProfile(userCredentials.user, {
        displayName: fullName,
      });
    })
    .then(() => {
      auth.signOut();
    })
    .catch((error) => Alert.alert(error.message));
};

// create question
export const addQuestionMethod = async (currentQuizId, question, optionOne, optionTwo, optionThree, optionFour, correctAnswer) => {
    const docRef = await addDoc(
        collection(db, "category", "4", "quiz", currentQuizId, "question"),
        {
          question: question,
          option: [
            {
              id: 0,
              type: "A",
              value: optionOne,
            },
            {
              id: 1,
              type: "B",
              value: optionTwo,
            },
            {
              id: 2,
              type: "C",
              value: optionThree,
            },
            {
              id: 3,
              type: "D",
              value: optionFour,
            },
          ],
          correctAnswer: correctAnswer,
        }
      );
}

// Add data to leaderboard
export const addLeaderboardMethod = async (catId, currentQuizId, id, name, correctCount, incorrectCount) => {
  const docRef = await addDoc(collection(db, "category", catId, "quiz", currentQuizId, "leaderboard" ),{
    userId: id,
    username: name,
    correctCount: correctCount,
    incorrectCount: incorrectCount
  })
}
