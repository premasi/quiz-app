import "react-native-gesture-handler";
import React, { useState, useEffect } from "react";
import { NavigationContainer } from "@react-navigation/native";
import AuthStackNavigator from "./src/routes/AuthStackNavigator";
import AppStackNavigator from "./src/routes/AppStackNavigator";
import { auth } from "./firebase/Index";

export default function App() {

  const [currentUser, setCurrentUser] = useState(null)

  const onAuthStateChanged = async user =>{
    await setCurrentUser(user)
  }

  useEffect(() => {
    const subcribe = auth.onAuthStateChanged(onAuthStateChanged)
    return subcribe;
  }, [])

  return(
    <NavigationContainer>
      {currentUser ? <AppStackNavigator /> : <AuthStackNavigator />}
    </NavigationContainer>
  );
}
