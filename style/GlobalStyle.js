import { StyleSheet } from "react-native";

export const globalStyle = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#20262E",
    alignItems: "center",
    justifyContent: "flex-start",
    padding: 20,
  },
  titleText: {
    color: "white",
    fontSize: 24,
    fontStyle: "bold",
    marginVertical: 32,
  },
  footer: {
    marginTop: 32,
    flexDirection: "row",
    alignItems: "center",
  },
  footerText: {
    color: "#E9E8E8",
    fontSize: 14,
  },
  footerTextPrimary: {
    color: "#913175",
    marginLeft: 4,
    fontSize: 14,
    fontWeight: "bold",
  },
});

export const globalStyle2 = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#20262E",
    position: "relative",
  },
  statusBarStyle: {
    backgroundColor: "white",
  },
  header: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    backgroundColor: "#913175",
    elevation: 4,
    paddingHorizontal: 20,
  },
  titleText: {
    color: "white",
    fontSize: 24,
    fontStyle: "bold",
    marginVertical: 16,
  },
  icon: {
    margin: 16,
  },
  buttonBackground: {
    flex: 1,
    alignItems: "flex-start",
    justifyContent: "center",
    borderRadius: 10,
    padding: 20,
  },
  icon2: {
    marginVertical: 10,
    marginLeft: -10,
  },
  titleTextQuiz:{
    flex: 2,
    fontSize: 16,
    color: '#E9E8E8'
  }
});
