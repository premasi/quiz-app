import { createStackNavigator } from "@react-navigation/stack";
import React from "react";
import Home from '../screens/home/Home';
import CreateQuiz from '../screens/home/CreateQuiz';
import CreateQuestion from '../screens/home/CreateQuestion';
import Quiz from "../screens/quiz/Quiz";
import PlayQuiz from "../screens/quiz/PlayQuiz";
import Leaderboard from "../screens/quiz/Leaderboard";

const Stack = createStackNavigator();

const AppStackNavigator = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
      }}
    >
      <Stack.Screen name="Home" component={Home} />
      <Stack.Screen name="CreateQuiz" component={CreateQuiz} />
      <Stack.Screen name="CreateQuestion" component={CreateQuestion} />
      <Stack.Screen name="Quiz" component={Quiz} />
      <Stack.Screen name="PlayQuiz" component={PlayQuiz} />
      <Stack.Screen name="Leaderboard" component={Leaderboard} />
    </Stack.Navigator>
  );
};

export default AppStackNavigator;
