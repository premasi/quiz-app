import {
  StyleSheet,
  Text,
  View,
  SafeAreaView,
  StatusBar,
  TouchableOpacity,
  FlatList,
  Dimensions,
} from "react-native";
import React, { useState, useEffect } from "react";
import { MaterialIcons } from "@expo/vector-icons";
import { collection, getDocs, query, orderBy, limit } from "firebase/firestore";
import { auth, db } from "../../../firebase/Index";
import { globalStyle, globalStyle2 } from "../../../style/GlobalStyle";

const Leaderboard = ({ navigation, route }) => {
  const [refresh, setRefresh] = useState(false);
  const [catId, setCatid] = useState(route.params.catId);
  const [quizId, setQuizId] = useState(route.params.quizId);
  const [quizTitle, setQuizTitle] = useState(route.params.quizTitle);
  const [rank, setRank] = useState([]);

  const getAllRank = async () => {

    const querySnapshot = await getDocs(query(
      collection(db, "category", catId, "quiz", quizId, "leaderboard"), orderBy("correctCount", "desc"), limit(10))
    );

    setRank(
      querySnapshot.docs.map((doc) => ({
        ...doc.data(),
        id: doc.id,
      }))
    );
  };

  useEffect(() => {
    getAllRank();
  }, []);

  return (
    <SafeAreaView style={globalStyle2.container}>
      <StatusBar
        style={globalStyle2.statusBarStyle}
        barStyle={"dark-content"}
      />

      {/* Header */}
      <View style={globalStyle2.header}>
        {/* Back Icon */}
        <TouchableOpacity
          style={globalStyle2.buttonBackground}
          onPress={() => navigation.goBack()}
        >
          <MaterialIcons
            style={globalStyle2.icon2}
            name="arrow-back"
            color={"#E9E8E8"}
            size={18}
          />
        </TouchableOpacity>
      </View>
      <View style={{ padding: 20 }}>
        <Text style={globalStyle.titleText}>Leaderboard {quizTitle}</Text>

        <FlatList
          data={rank}
          keyExtractor={(item) => item.id}
          refreshing={refresh}
          onRefresh={getAllRank}
          showsVerticalScrollIndicator={false}
          renderItem={({ item, index }) => (
            <View style={styles.rankContainer}>
              <Text style={styles.textName}>{index + 1}. {item.username}</Text>
              <Text style={styles.countText}>{item.correctCount}</Text>
            </View>
          )}
        />
      </View>
    </SafeAreaView>
  );
};

export default Leaderboard;

const deviceWidth = Math.round(Dimensions.get("window").width);
const styles = StyleSheet.create({
  rankContainer: {
    alignSelf: "center",
    padding: 10,
    width: deviceWidth - 20,
    backgroundColor: "#E9E8E8",
    borderRadius: 20,
    borderColor: "#913175",
    borderWidth: 5,
    marginTop: 16,
    flexDirection: "row",
    alignContent: "center",
    justifyContent: 'space-between'
  },
  textName: {
    fontSize: 16,
    alignSelf: 'flex-start',
    marginLeft: 8
  },
  countText:{
    alignSelf: 'flex-end',
    marginRight: 8,
    fontSize: 16,
    color: 'green'
  }
});
