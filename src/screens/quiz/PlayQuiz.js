import {
  StyleSheet,
  Text,
  View,
  SafeAreaView,
  StatusBar,
  TouchableOpacity,
  FlatList,
} from "react-native";
import React, { useState, useEffect } from "react";
import { collection, getDocs } from "firebase/firestore";
import { auth, db } from "../../../firebase/Index";
import { MaterialIcons } from "@expo/vector-icons";
import FormButton from "../../components/shared/FormButton";
import ResultModal from "../../components/modal/ResultModal";
import { globalStyle2 } from "../../../style/GlobalStyle";
import Countdown from "react-native-countdown-component";
import { addLeaderboardMethod } from "../../../firebase/Logic";

const PlayQuiz = ({ navigation, route }) => {
  // data
  const [refresh, setRefresh] = useState(false);
  const [catId, setCatId] = useState(route.params.catId);
  const [quizId, setQuizId] = useState(route.params.quizId);
  const [quizTitle, setQuizTitle] = useState(route.params.quizTitle);
  const [allQuestion, setAllQuestion] = useState([]);

  // count answer
  const [correctCount, setCorrectCount] = useState(0);
  const [incorrectCount, setIncorrectCount] = useState(0);

  // modal status
  const [modalVisibleStatus, setModalVisibleStatus] = useState(false);

  //timer
  // const [timer, setTimer] = useState(600);

  // get all question
  const getAllQuestion = async () => {
    const querySnapshot = await getDocs(
      collection(db, "category", catId, "quiz", quizId, "question")
    );

    setAllQuestion(
      querySnapshot.docs.map((doc) => ({
        ...doc.data(),
        id: doc.id,
      }))
    );
  };

  //change option color when clicked
  const optionTextColor = (currentQuestion, currentOption) => {
    if (currentQuestion.selectedOption) {
      if (currentOption == currentQuestion.selectedOption) {
        if (currentOption.id == currentQuestion.correctAnswer) {
          return "green";
        } else {
          return "red";
        }
      } else {
        return "black";
      }
    } else {
      return "black";
    }
  };

  //get all question
  useEffect(() => {
    getAllQuestion();
  }, []);

  return (
    <SafeAreaView style={globalStyle2.container}>
      {/* Status Bar */}
      <StatusBar
        style={globalStyle2.statusBarStyle}
        barStyle={"dark-content"}
      />

      {/* Header */}
      <View style={globalStyle2.header}>
        {/* Back Icon */}
        <TouchableOpacity
          style={globalStyle2.buttonBackground}
          onPress={() => navigation.goBack()}
        >
          <MaterialIcons
            style={globalStyle2.icon2}
            name="arrow-back"
            color={"#E9E8E8"}
            size={18}
          />
        </TouchableOpacity>

        {/* Title */}
        <Text style={globalStyle2.titleTextQuiz}>{quizTitle}</Text>

        {/* Correct Count */}
        <View style={styles.correctContainer}>
          <MaterialIcons name="check" color={"#E9E8E8"} size={14} />
          <Text style={styles.correctText}>{correctCount}</Text>
        </View>

        {/* Incorrect Count */}
        <View style={styles.incorrectContainer}>
          <MaterialIcons name="close" color={"#E9E8E8"} size={14} />
          <Text style={styles.incorrectText}>{incorrectCount}</Text>
        </View>
      </View>

      {/* Countdown */}
      {/* <View style={styles.countdownContainer}>
        <Countdown
          size={20}
          until={timer}
          onFinish={() => setModalVisibleStatus(true)}
        />
      </View> */}

      {/* Question */}
      <FlatList
        data={allQuestion}
        onRefresh={getAllQuestion}
        refreshing={refresh}
        showsVerticalScrollIndicator={false}
        keyExtractor={(item) => item.id}
        renderItem={({ item, index }) => (
          <View style={styles.listContainer}>
            <Text
              style={{
                color: "white",
                fontSize: 16,
                marginBottom: 8,
                fontStyle: "bold",
              }}
            >
              {index + 1}. {item.question}
            </Text>

            {/* Map option */}
            {item?.option.map((items, i) => (
              <TouchableOpacity
                key={i}
                style={styles.buttonStyle}
                onPress={() => {
                  if (item.selectedOption) {
                    return null;
                  }
                  if (items.id === item.correctAnswer) {
                    setCorrectCount(correctCount + 1);
                  } else {
                    setIncorrectCount(incorrectCount + 1);
                  }

                  let tempQuestion = [...allQuestion];
                  tempQuestion[index].selectedOption = items;
                  setAllQuestion([...tempQuestion]);
                }}
              >
                <Text
                  style={{
                    color: optionTextColor(item, items),
                    fontSize: 14,
                    fontStyle: "bold",
                  }}
                >
                  {items.type}. {items.value}
                </Text>
              </TouchableOpacity>
            ))}
          </View>
        )}
        ListFooterComponent={() => (
          <FormButton
            label="Submit"
            style={{ margin: 10 }}
            handleOnPress={() => {
              //set modal status
              setModalVisibleStatus(true);
            }}
          />
        )}
      />

      {/* Modal */}
      <ResultModal
        modalVisible={modalVisibleStatus}
        correctCount={correctCount}
        incorrectCount={incorrectCount}
        totalCount={allQuestion.length}
        handleOnClose={() => {
          setModalVisibleStatus(false);
        }}
        handleRetry={() => {
          addLeaderboardMethod(catId, quizId, auth.currentUser.uid, auth.currentUser.displayName, correctCount, incorrectCount)
          setCorrectCount(0);
          setIncorrectCount(0);
          getAllQuestion();
          setModalVisibleStatus(false);
        }}
        handleHome={() => {
          addLeaderboardMethod(catId, quizId, auth.currentUser.uid, auth.currentUser.displayName, correctCount, incorrectCount)
          navigation.navigate("Home");
          setModalVisibleStatus(false);
        }}
        handleLeaderboard={() => {
          addLeaderboardMethod(catId, quizId, auth.currentUser.uid, auth.currentUser.displayName, correctCount, incorrectCount)
          navigation.replace("Leaderboard", {
            catId: catId,
            quizId: quizId,
            quizTitle: quizTitle
          });
          setModalVisibleStatus(false);
        }}
      />
    </SafeAreaView>
  );
};

export default PlayQuiz;

const styles = StyleSheet.create({
  countdownContainer: {
    marginVertical: 5,
    backgroundColor: "#E9E8E8",
  },
  correctContainer: {
    flex: 0.2,
    backgroundColor: "green",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    paddingHorizontal: 10,
    paddingVertical: 4,
    borderTopLeftRadius: 5,
    borderBottomLeftRadius: 5,
  },
  listContainer: {
    padding: 20,
    borderRadius: 10,
    marginVertical: 5,
    marginHorizontal: 10,
    alignItems: "flex-start",
    justifyContent: "flex-start",
    backgroundColor: "#913175",
    elevation: 2,
  },
  correctText: {
    color: "#E9E8E8",
    marginLeft: 5,
  },
  incorrectContainer: {
    flex: 0.2,
    backgroundColor: "red",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    paddingHorizontal: 10,
    paddingVertical: 4,
    borderTopRightRadius: 5,
    borderBottomRightRadius: 5,
  },
  incorrectText: {
    color: "#E9E8E8",
    marginLeft: 5,
  },
  buttonStyle: {
    marginTop: 8,
    backgroundColor: "#E9E8E8",
    width: "100%",
    padding: 20,
    borderRadius: 20,
    alignItems: "flex-start",
    justifyContent: "flex-start",
  },
  quizCount: {
    padding: 20,
    alignSelf: "flex-end",
    fontSize: 14,
    color: "#E9E8E8",
  },
});
