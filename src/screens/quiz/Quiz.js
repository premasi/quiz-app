import {
  StyleSheet,
  Text,
  View,
  SafeAreaView,
  StatusBar,
  TouchableOpacity,
  FlatList,
  Dimensions,
} from "react-native";
import React, { useState, useEffect } from "react";
import { MaterialIcons } from "@expo/vector-icons";
import { collection, getDocs } from "firebase/firestore";
import { db } from "../../../firebase/Index";
import { auth } from "../../../firebase/Index";
import { globalStyle2 } from "../../../style/GlobalStyle";

const Quiz = ({ navigation, route }) => {
  const [catid, setCatId] = useState(route.params.currentCatId);
  const [allQuiz, setAllQuiz] = useState("");
  const [refresh, setRefresh] = useState(false);

  const handleSignOut = () => {
    auth
      .signOut()
      .catch((error) => Alert.alert("Log Out Failed", error.message));
  };

  const getAllQuiz = async () => {
    const querySnapshot = await getDocs(
      collection(db, "category", catid, "quiz")
    );

    setAllQuiz(
      querySnapshot.docs.map((doc) => ({
        ...doc.data(),
        id: doc.id,
      }))
    );
  };

  useEffect(() => {
    getAllQuiz();
  }, []);

  return (
    <SafeAreaView style={globalStyle2.container}>
      <StatusBar style={globalStyle2.statusBarStyle} barStyle={"dark-content"} />

      <View style={globalStyle2.header}>
        <Text style={globalStyle2.titleText}>Quiz App</Text>
        <TouchableOpacity
          style={styles.buttonBackground}
          onPress={handleSignOut}
        >
          <MaterialIcons
            style={globalStyle2.icon}
            name="logout"
            color={"#913175"}
            size={18}
          />
        </TouchableOpacity>
      </View>

      <FlatList
        data={allQuiz}
        onRefresh={getAllQuiz}
        refreshing={refresh}
        showsVerticalScrollIndicator={false}
        renderItem={({ item: quiz }) => (
          <View style={styles.listContainer}>
            <View style={{ padding: 20, flex: 2 }}>
              <Text style={{ fontSize: 18, fontStyle: "bold" }}>
                {quiz.title}
              </Text>
              <Text style={{ opacity: 0.7 }}>By {quiz.authorName}</Text>
              {quiz.description != "" ? (
                <Text style={{ opacity: 0.5 }}>{quiz.description}</Text>
              ) : null}
            </View>

            <View style={{ padding: 20, flex: 1 }}>
              <MaterialIcons name='leaderboard' size={18} style={styles.icon2} onPress={() => navigation.navigate('Leaderboard', {
                catId: catid,
                quizId: quiz.id,
                quizTitle: quiz.title
              })}/>
              <TouchableOpacity style={styles.buttonStyle} onPress={() => {
                navigation.navigate('PlayQuiz', {
                    catId: catid,
                    quizId: quiz.id,
                    quizTitle: quiz.title,
                })
              }}>
                <Text
                  style={{ fontSize: 18, fontStyle: "bold", color: "white" }}
                >
                  Start
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        )}
      />
    </SafeAreaView>
  );
};

export default Quiz;

const deviceWidth = Math.round(Dimensions.get("window").width);
const styles = StyleSheet.create({
  buttonContainer: {
    marginTop: 20,
    alignItems: "flex-end",
  },
  buttonBackground: {
    backgroundColor: "#E9E8E8",
    alignItems: "center",
    borderWidth: 2,
    borderRadius: 10,
    borderColor: "#913175",
    marginTop: 10,
    marginBottom: 10,
  },
  listContainer: {
    alignSelf: "center",
    padding: 10,
    width: deviceWidth - 20,
    backgroundColor: "#E9E8E8",
    borderRadius: 20,
    borderColor: "#913175",
    borderWidth: 3,
    marginTop: 16,
    justifyContent: "space-between",
    flexDirection: "row",
    alignContent: "center",
  },
  buttonStyle: {
    flex: 1,
    backgroundColor: "#913175",
    borderRadius: 20,
    alignItems: "center",
    justifyContent: "center",
  }, 
  icon2: {
    alignSelf: 'flex-end',
    marginBottom: 8,
  }
})
