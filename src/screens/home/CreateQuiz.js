import {Text, SafeAreaView, Alert } from "react-native";
import React, { useState } from "react";
import FormInput from "../../components/shared/FormInput";
import FormButton from "../../components/shared/FormButton";
import { collection, addDoc } from "firebase/firestore";
import { auth, db } from "../../../firebase/Index";
import { globalStyle } from "../../../style/GlobalStyle";

const CreateQuiz = ({ navigation }) => {
  const [title, setTitle] = useState("");
  const [desc, setDesc] = useState("");

  const saveQuizHandler = async () => {
    if (title != "") {
      try {
        const docRef = await addDoc(collection(db, "category", "4", "quiz"), {
          title: title,
          description: desc,
          authorId: auth.currentUser.uid,
          authorName: auth.currentUser.displayName,
        });
        console.log("Document written with ID: ", docRef.id);
        setTitle("");
        setDesc("");
        navigation.replace("CreateQuestion", {
          currentQuizId: docRef.id,
          currentQuizTitle: title,
        });
      } catch (e) {
        console.error("Error adding document: ", e);
      }
    } else {
      Alert.alert("Warning", "Title cannot be empty");
    }
  };

  return (
    <SafeAreaView style={globalStyle.container}>
      {/* Header */}
      <Text style={globalStyle.titleText}>Create Quiz</Text>

      {/* Title */}
      <FormInput
        label="Title"
        placeholder="Enter your title"
        onChangeText={(value) => setTitle(value)}
        value={title}
        keyboardType="default"
      />

      {/* Description */}
      <FormInput
        label="Description"
        placeholder="Enter description"
        onChangeText={(value) => setDesc(value)}
        value={desc}
        keyboardType="default"
      />

      {/* Submit Button */}
      <FormButton
        label="Save"
        handleOnPress={saveQuizHandler}
        style={{ width: "100%", marginTop: 32 }}
      />
    </SafeAreaView>
  );
};

export default CreateQuiz;