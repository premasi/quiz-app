import {
  StyleSheet,
  Text,
  View,
  SafeAreaView,
  TouchableOpacity,
  Alert,
  FlatList,
  StatusBar
} from "react-native";
import React, { useState, useEffect } from "react";
import { auth } from "../../../firebase/Index";
import { MaterialIcons } from "@expo/vector-icons";
import { collection, getDocs } from "firebase/firestore";
import { db } from "../../../firebase/Index";
import FormButton from "../../components/shared/FormButton";

const Home = ({ navigation }) => {
  const [category, setCategory] = useState("");
  const [refresh, setReferesh] = useState(false)

  const handleSignOut = () => {
    auth
      .signOut()
      .catch((error) => Alert.alert("Log Out Failed", error.message));
  };

  const getCategory = async () => {
    const querySnapshot = await getDocs(collection(db, "category"));

    setCategory(
      querySnapshot.docs.map((doc) => ({
        ...doc.data(),
        id: doc.id,
      }))
    );
  };

  useEffect(() => {
    getCategory();
  }, []);

  return (
    <SafeAreaView style={styles.container}>
      {/* Status Bar */}
      <StatusBar style={styles.statusBarStyle} barStyle={"dark-content"} />

      {/* Header */}
      <View style={styles.header}>
        <Text style={styles.titleText}>Quiz App</Text>
        <TouchableOpacity
          style={styles.buttonBackground}
          onPress={handleSignOut}
        >
          <MaterialIcons
            style={styles.icon}
            name="logout"
            color={"#913175"}
            size={18}
          />
        </TouchableOpacity>
      </View>

      {/* Category */}
      <View style={{ padding: 20 }}>
        <Text style={styles.titleText}>Category</Text>

        <FlatList
          numColumns={2}
          keyExtractor={(item) => item.id}
          refreshing={refresh}
          onRefresh={getCategory}
          showsVerticalScrollIndicator={false}
          data={category}
          renderItem={({ item }) => (
            <TouchableOpacity style={styles.listContainer} onPress={() => navigation.navigate('Quiz', {currentCatId: item.id})}>
              <Text style={styles.appButtonText}>{item.title}</Text>
            </TouchableOpacity>
          )}
        />
      </View>

      {/* Create Quiz */}
      <View style={{padding: 20}}>
        <Text style={styles.titleText}>Create Quiz</Text>
        <FormButton
          label="Create New Quiz"
          handleOnPress={() => {
            navigation.navigate("CreateQuiz");
          }}
        />
      </View>
    </SafeAreaView>
  );
};

export default Home;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#20262E",
    position: "relative",
  },
  header: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: '#913175',
    elevation: 4,
    paddingHorizontal: 20
  },
  listContainer: {
    padding: 20,
    borderRadius: 10,
    marginVertical: 5,
    marginHorizontal: 10,
    alignItems: "center",
    justifyContent: "space-between",
    backgroundColor: "#913175",
    elevation: 2,
  },
  buttonContainer: {
    marginTop: 20,
    alignItems: "flex-end",
  },
  buttonBackground: {
    backgroundColor: "#E9E8E8",
    alignItems: "center",
    borderWidth: 2,
    borderRadius: 10,
    borderColor: "#913175",
    marginTop: 10,
    marginBottom: 10,
  },
  icon: {
    margin: 16,
  },
  titleText: {
    color: "white",
    fontSize: 24,
    fontStyle: "bold",
    marginVertical: 16,
  },
  appButtonContainer: {
    flex: 1,
    padding: 10,
  },
  appButtonText: {
    fontSize: 18,
    color: "#E9E8E8",
    fontWeight: "bold",
    alignSelf: "center",
  },
});
