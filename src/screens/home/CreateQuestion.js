import {
  StyleSheet,
  Text,
  View,
  KeyboardAvoidingView,
  ScrollView,
  Alert,
  TouchableOpacity,
} from "react-native";
import React, { useState } from "react";
import FormInput from "../../components/shared/FormInput";
import FormButton from "../../components/shared/FormButton";
import { addQuestionMethod } from "../../../firebase/Logic";
import { globalStyle } from "../../../style/GlobalStyle";

const CreateQuestion = ({ navigation, route }) => {
  const [currentQuizId, setCurrentQuizId] = useState(
    route.params.currentQuizId
  );
  const [currentQuizTitle, setCurrentQuizTitle] = useState(
    route.params.currentQuizTitle
  );
  const [question, setQuestion] = useState("");
  const [optionOne, setOptionOne] = useState("");
  const [optionTwo, setOptionTwo] = useState("");
  const [optionThree, setOptionThree] = useState("");
  const [optionFour, setOptionFour] = useState("");
  const [status, setStatus] = useState("");
  const [correctAnswer, setCorrectAnswer] = useState("");

  const getCorrectAnswer = (number) => {
    setStatus(number);
    setCorrectAnswer(number - 1);
  };

  const saveQuestionHandle = () => {
    if (
      question != "" ||
      optionOne != "" ||
      optionTwo != "" ||
      optionThree != "" ||
      optionFour != "" ||
      correctAnswer != ""
    ) {
      addQuestionMethod(
        currentQuizId,
        question,
        optionOne,
        optionTwo,
        optionThree,
        optionFour,
        correctAnswer
      );
      setQuestion("");
      setOptionOne("");
      setOptionTwo("");
      setOptionThree("");
      setOptionFour("");
      setCorrectAnswer("");
      setStatus("");
    } else {
      Alert.alert("Warning", "All field cannot be empty");
    }
  };

  return (
    <KeyboardAvoidingView style={{ flex: 1 }}>
      <ScrollView style={styles.scrollViewStyle}>
        <View style={globalStyle.container}>
          {/* Title */}
          <Text style={globalStyle.titleText}>{currentQuizTitle}</Text>

          {/* Question */}
          <FormInput
            label="Question"
            placeholder="Enter question"
            onChangeText={(val) => setQuestion(val)}
            value={question}
          />
        </View>

        {/* Answer */}
        <View style={globalStyle.container}>
          <FormInput
            label="Answer 1"
            placeholder="Enter answer 1"
            onChangeText={(val) => setOptionOne(val)}
            value={optionOne}
          />
          <FormInput
            label="Answer 2"
            placeholder="Enter answer 2"
            onChangeText={(val) => setOptionTwo(val)}
            value={optionTwo}
          />
          <FormInput
            label="Answer 3"
            placeholder="Enter answer 3"
            onChangeText={(val) => setOptionThree(val)}
            value={optionThree}
          />
          <FormInput
            label="Answer 4"
            placeholder="Enter answer 4"
            onChangeText={(val) => setOptionFour(val)}
            value={optionFour}
          />
          <View style={styles.wrapper}>
            {["1", "2", "3", "4"].map((number) => (
              <View key={number} styles={styles.mood}>
                <Text style={styles.numberText}>{number}</Text>
                <TouchableOpacity
                  style={styles.outer}
                  onPress={() => getCorrectAnswer(number)}
                >
                  {status === number && <View style={styles.inner} />}
                </TouchableOpacity>
              </View>
            ))}
          </View>

          {/* Button */}
          <FormButton
            label="Save Question"
            handleOnPress={saveQuestionHandle}
            style={{ width: "100%", marginTop: 32 }}
          />
          <FormButton
            label="Done"
            style={{ width: "100%", marginTop: 32 }}
            isPrimary={false}
            handleOnPress={() => {
              navigation.replace("Home");
            }}
          />
        </View>
      </ScrollView>
    </KeyboardAvoidingView>
  );
};

export default CreateQuestion;

const styles = StyleSheet.create({
  scrollViewStyle: {
    flex: 1, 
    backgroundColor: "#20262E"
  },
  wrapper: {
    flexDirection: "row",
    justifyContent: "space-evenly",
    marginTop: 15,
  },
  outer: {
    width: 25,
    height: 25,
    borderColor: "#E9E8E8",
    borderWidth: 1,
    borderRadius: 15,
    justifyContent: "center",
    alignItems: "center",
    marginHorizontal: 15,
  },
  inner: {
    width: 15,
    height: 15,
    backgroundColor: "#913175",
    borderRadius: 10,
  },
  mood: {
    alignItems: "center",
    justifyContent: "center",
  },
  numberText: {
    width: 25,
    height: 25,
    marginHorizontal: 22,
    fontSize: 16,
    color: "#E9E8E8",
  },
});
