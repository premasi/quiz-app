import { SafeAreaView, Text, View } from "react-native";
import React, { useState, useEffect } from "react";
import FormInput from "../../components/shared/FormInput";
import FormButton from "../../components/shared/FormButton";
import { auth } from "../../../firebase/Index";
import { SignInMethod } from "../../../firebase/Logic";
import { globalStyle } from "../../../style/GlobalStyle";

const SignIn = ({ navigation }) => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const submitHandler = () => {
    if (email != "" || password != "") {
      SignInMethod(auth, email, password);
    }
  };

  useEffect(() => {
    auth.onAuthStateChanged((user) => {
      if (user) {
      }
    });
  }, []);

  return (
    <SafeAreaView style={globalStyle.container}>
      {/* Header */}
      <Text style={globalStyle.titleText}>Sign In</Text>

      {/* Email */}
      <FormInput
        label="Email"
        placeholder="asepTheDragon@gmail.com"
        onChangeText={(value) => setEmail(value)}
        value={email}
        keyboardType={"email-address"}
      />

      {/* Password */}
      <FormInput
        label="Password"
        placeholder="Enter your password"
        onChangeText={(value) => setPassword(value)}
        value={password}
        keyboardType="default"
        secureTextEntry={true}
      />

      {/* Submit Button */}
      <FormButton
        label="Login"
        handleOnPress={submitHandler}
        style={{ width: "100%", marginTop: 32 }}
      />

      {/* Footer */}
      <View style={globalStyle.footer}>
        <Text style={globalStyle.footerText}>Don't have account? </Text>
        <Text
          style={globalStyle.footerTextPrimary}
          onPress={() => navigation.navigate("SignUp")}
        >
          Click Here
        </Text>
      </View>
    </SafeAreaView>
  );
};

export default SignIn;
