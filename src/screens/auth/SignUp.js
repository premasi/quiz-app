import {
  Text,
  View,
  SafeAreaView,
  Alert,
} from "react-native";
import React, { useState } from "react";
import FormInput from "../../components/shared/FormInput";
import FormButton from "../../components/shared/FormButton";
import { auth } from "../../../firebase/Index";
import { ScrollView } from "react-native-gesture-handler";
import { SignUpMethod } from "../../../firebase/Logic";
import { globalStyle } from "../../../style/GlobalStyle";

const SignUp = ({ navigation }) => {
  const [fullName, setFullName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");

  const signUpSubmitHandler = () => {
    if (fullName != "" && email != "" && password != "") {
      if (confirmPassword != password) {
        Alert.alert(
          "Registration Failed",
          "Confirm password and password is not matched"
        );
      } else {
        // sign up
        SignUpMethod(auth, fullName, email, password)
      }
    } else {
      Alert.alert("Registration Failed", "Field cannot be empty");
    }
  };

  return (
    <ScrollView>
      <SafeAreaView style={globalStyle.container}>
        {/* Header */}
        <Text style={globalStyle.titleText}>Sign Up</Text>

        {/* Full Name */}
        <FormInput
          label="Full Name"
          placeholder="Asep Dragnil"
          onChangeText={(value) => setFullName(value)}
          value={fullName}
          keyboardType={"default"}
        />

        {/* Email */}
        <FormInput
          label="Email"
          placeholder="asepTheDragon@gmail.com"
          onChangeText={(value) => setEmail(value)}
          value={email}
          keyboardType={"email-address"}
        />

        {/* Password */}
        <FormInput
          label="Password"
          placeholder="Enter your password"
          onChangeText={(value) => setPassword(value)}
          value={password}
          keyboardType="default"
          secureTextEntry={true}
        />

        {/* Confirm Password */}
        <FormInput
          label="Confirm Password"
          placeholder="Enter your confirm password"
          onChangeText={(value) => setConfirmPassword(value)}
          value={confirmPassword}
          keyboardType="default"
          secureTextEntry={true}
        />

        {/* Submit Button */}
        <FormButton
          label="Register"
          handleOnPress={signUpSubmitHandler}
          style={{ width: "100%", marginTop: 32 }}
        />

        {/* Footer */}
        <View style={globalStyle.footer}>
          <Text style={globalStyle.footerText}>Already have account? </Text>
          <Text
            style={globalStyle.footerTextPrimary}
            onPress={() => navigation.goBack()}
          >
            Sign In
          </Text>
        </View>
      </SafeAreaView>
    </ScrollView>
  );
};

export default SignUp;
