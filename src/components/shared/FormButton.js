import { StyleSheet, Text, TouchableOpacity } from 'react-native'
import React from 'react'

const FormButton = ({label='', handleOnPress=null, style, isPrimary=true, ...more}) => {
  return (
    <TouchableOpacity style={
        {
            padding: 16,
            backgroundColor: isPrimary ? '#913175' : "#E9E8E8",
            borderRadius: 5,
            borderWidth: 1,
            borderColor: '#913175',
            ...style
        }
    }
        activeOpacity={0.9}
        onPress={handleOnPress}
    >
        <Text style={{
            color: isPrimary ? "#E9E8E8" : '#913175',
            textAlign: 'center',
            fontSize: 16
        }}>{label}</Text>
    </TouchableOpacity>
  )
}

export default FormButton

const styles = StyleSheet.create({

})