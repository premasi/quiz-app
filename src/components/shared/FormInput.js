import { StyleSheet, Text, View, TextInput } from 'react-native'
import React from 'react'

const FormInput = ({label='', placeholder='', onChangeText=null, value=null, ...more}) => {
  return (
    <View style={styles.formContainer}>
      <Text style={styles.labelStyle}>{label}</Text>
      <TextInput 
        style={styles.formStyle} 
        placeholder={placeholder} 
        placeholderTextColor='#696969'
        onChangeText={onChangeText}
        value={value}
        {...more}/>
    </View>
  )
}

export default FormInput

const styles = StyleSheet.create({
    formContainer:{
        width: '100%',
        margin: 16
    },
    labelStyle:{
        color: '#E9E8E8',
        fontStyle: 'bold',
        fontSize: 16
    },
    formStyle: {
        color: '#E9E8E8',
        padding: 10,
        borderColor: '#913175',
        borderWidth: 3,
        borderRadius: 10,
        width: '100%',
        marginTop: 8
    }
})