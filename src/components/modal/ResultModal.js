import { Modal, StyleSheet, Text, View, TouchableOpacity } from "react-native";
import React from "react";
import { MaterialIcons } from "@expo/vector-icons";

const ResultModal = ({
  modalVisible,
  correctCount,
  incorrectCount,
  totalCount,
  handleOnClose,
  handleRetry,
  handleHome,
  handleLeaderboard
}) => {
  return (
    <Modal
      animationType="slide"
      transparent={true}
      visible={modalVisible}
      onRequestClose={handleOnClose}
    >
      <View style={styles.modalContainer}>
        <View style={styles.container}>
          {/* Title */}
          <Text style={styles.textModalTitleStyle}>Result</Text>

          {/* Count Text */}
          <View style={styles.bodyCountContainer}>
            <View style={styles.countContainer}>
              <Text style={styles.numberCorrrectText}>{correctCount}</Text>
              <Text>Correct</Text>
            </View>
            <View style={styles.countContainer}>
              <Text style={styles.numberIncorrrectText}>{incorrectCount}</Text>
              <Text>Incorrect</Text>
            </View>
          </View>

          {/* Show unanswered question remain */}
          <Text style={styles.totalNotAnsweredText}>
            {totalCount - (incorrectCount + correctCount)} Question is not
            answered
          </Text>

          {/* Try Again Button*/}
          <TouchableOpacity
            style={styles.tryAgainButtonStyle}
            onPress={handleRetry}
          >
            <MaterialIcons name="replay" color={"#E9E8E8"} />
            <Text style={styles.tryAgainText}>Try Again</Text>
          </TouchableOpacity>

          {/* Go Home Button*/}
          <TouchableOpacity
            style={styles.goHomeButtonStyle}
            onPress={handleHome}
          >
            <MaterialIcons name="home" color={"#913175"} />
            <Text style={styles.goHomeText}>Back to Home</Text>
          </TouchableOpacity>

          {/* Leaderboard*/}
          <TouchableOpacity
            style={styles.leaderboardButtonStyle}
            onPress={handleLeaderboard}
          >
            <MaterialIcons name="leaderboard" color={"#E9E8E8"} />
            <Text style={styles.leaderboardText}>Leaderboard</Text>
          </TouchableOpacity>
        </View>
      </View>
    </Modal>
  );
};

export default ResultModal;

const styles = StyleSheet.create({
  modalContainer: {
    flex: 1,
    backgroundColor: "rgba(52, 52, 52, 0.8)",
    justifyContent: "center",
    alignItems: "center",
  },
  container: {
    backgroundColor: "white",
    width: "90%",
    borderRadius: 5,
    padding: 40,
    alignItems: "center",
  },
  textModalTitleStyle: {
    fontSize: 30,
    color: "black",
  },
  bodyCountContainer: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
  },
  countContainer: {
    alignItems: "center",
    padding: 20,
  },
  numberCorrrectText: {
    color: "green",
    fontSize: 20,
  },
  numberIncorrrectText: {
    color: "red",
    fontSize: 20,
  },
  totalNotAnsweredText: {
    opacity: 0.8,
  },
  tryAgainButtonStyle: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    marginTop: 16,
    padding: 10,
    backgroundColor: "#913175",
    paddingHorizontal: 57,
    borderRadius: 20,
  },
  tryAgainText: {
    color: "#E9E8E8",
    marginLeft: 8,
  },
  goHomeButtonStyle: {
    borderWidth: 1,
    borderColor: "#913175",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    padding: 10,
    marginTop: 16,
    backgroundColor: "#E9E8E8",
    paddingHorizontal: 40,
    borderRadius: 20,
  },
  goHomeText: {
    color: "#913175",
    marginLeft: 8,
  },
  leaderboardButtonStyle: {
    borderWidth: 1,
    borderColor: "#913175",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    padding: 10,
    marginTop: 16,
    backgroundColor: "#CD5888",
    paddingHorizontal: 44,
    borderRadius: 20,
  },
  leaderboardText: {
    color: "#E9E8E8",
    marginLeft: 8,
  },
});
